
class Rapot {

    maxminaverageSum(arr) {
        let max = arr[0];
        let min = arr[0];
        let sum = arr[0];
        let l = 0;
        let tl = 0;
        arr.forEach(function (value) {
            if (value > max) max = value;
            if (value < min) min = value;
            if (value >= 75) l++;
            if (value < 75) tl++;
            sum += value;
        })
        let avg = sum / arr.length;
        return `Nilai tertinggi : ${max}\nNilai terendah : ${min}\nRata-rata : ${avg}\nSiswa Lulus : ${l} \nSiswa Tidak Lulus : ${tl}`;
    }

}

module.exports = Rapot;